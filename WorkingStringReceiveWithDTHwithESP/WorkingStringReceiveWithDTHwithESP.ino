#include <SoftwareSerial.h>

#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

struct {
   //float f = 0;
   float h = 0;
   float t = 0;
   uint8_t mic = 0;
   float co2 = 0;
   float ldrLux = 0;
   float BMP = 0;
  } myData;

unsigned long lastMilis = 0;


RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;


SoftwareSerial esp(7,8);// RX, TX


void setup(void){
  Serial.begin(9600);
  radio.begin();
  radio.setPALevel(RF24_PA_MAX);
  radio.openReadingPipe(1,pipe);
  radio.startListening();
  esp.begin(9600);
  delay(2000);
  sendCommand("AT+RST", 3000, 2, "ready");
  checkConnection();
      
  lastMilis = millis();
}
void loop(void){


  
   if(radio.available()){
      while(radio.available()){
        radio.read(&myData, sizeof(myData)); 
               
                  Serial.print("Humidity: ");
                  Serial.print(myData.h);
                  Serial.print(" %\t");
                  Serial.print("Temperature: ");
                  Serial.print(myData.t);
                  Serial.print(" *C ");
                  Serial.print(F("Co2: "));
                  Serial.print(myData.co2);
                  Serial.print(F("mg/L \t"));
                  Serial.print(F("Noise: "));
                  Serial.println(myData.mic);
                  Serial.print(F("LDR: "));
                  Serial.println(myData.ldrLux);
                  Serial.print(F("BMP: "));
                  Serial.println(myData.BMP);                  
                  Serial.flush();            
        }
        
    }

 if ( (millis() - lastMilis) >= 120000 ){
      sendData();  
      Serial.println(F("Data Sent using wifi"));
      delay(3000);      
      Serial.flush();
      esp.flush();
      lastMilis = millis();
            
  }

    
}


void sendData(){

  checkConnection();
  sendCommand("AT+CIPMUX=1", 2000, 3, "OK");
  
  String cmd = "AT+CIPSTART=1,\"TCP\",\"api.thingspeak.com\",80";
  String keyAndData = "";
  sendCommand(cmd, 5000, 3, "OK");
  
  cmd = "AT+CIPSEND=1,";
  
  keyAndData+= "GET /update?api_key=FTV82AAH69I9TXG4&field1=";
  keyAndData+=myData.t;
  keyAndData+="&field2=";
  keyAndData+=myData.h;
  keyAndData+="&field3=";
  keyAndData+=myData.mic;
  keyAndData+="&field4=";
  keyAndData+=myData.co2;
  keyAndData+="&field5=";
  keyAndData+=myData.BMP;
  keyAndData+="&field6=";
  keyAndData+=myData.ldrLux;
  keyAndData+= " HTTP/1.0\r\nHostname: api.thingspeak.com\r\n\r\n"; 
  cmd+=keyAndData.length();
  sendCommand(cmd.c_str(), 3000, 5, "OK");
  
  sendCommand(keyAndData.c_str(), 5000, 3, "OK");
  
  sendCommand("AT+CIPCLOSE=1", 5000, 2, "ERROR");
  
  }

bool checkConnection(){
    Serial.println(F("Checking Connection..."));
    digitalWrite(13, LOW); // checking Connection 
    esp.flush();
    delay(1);
    sendCommand("AT+CWMODE=1",5000, 3, "OK");
    sendCommand("AT+CWQAP",1000, 2, "WIFI DISCONNECT");
    sendCommand("AT+CWJAP?",1000,3,"No AP");
    bool isConnect = false;
    
    while(!isConnect){
        esp.flush();
        delay(1);
        Serial.flush();
        delay(1);
        sendCommand("AT+CWJAP=\"OCTRVES-PTCL\",\"pakistan123\"", 5000, 5, "OK");
        isConnect = sendCommand("AT+CWJAP?", 2000, 3, "+CWJAP:\"");
        
        delay(500);        
      }

      if(isConnect){
          digitalWrite(13, HIGH); // Connection succesful
          return true;
      }else{
          return false;
        }      
  }

bool sendCommand(const char* c, unsigned long timeout, uint8_t retries, const char* waitingFor){
      esp.flush();
      Serial.flush();
      Serial.println(F("-------------------- START-------------------"));
      Serial.print(F("Command Sent: "));
      Serial.println(c);
      Serial.print(F("Waiting for reply : "));
      Serial.println(waitingFor);
      unsigned long currentTime = 0;
      bool isCommandSent = false;
      for(uint8_t i = retries; i > 0 && !isCommandSent ; i--){
        String ESPReply = "";
            esp.println(c);
            delay(5);
            currentTime = millis();
            
            while( (millis() - currentTime) <= timeout){
                  while(esp.available()){
                        char c = esp.read();
                        ESPReply += c;
                        delay(1);
             }
                 
                 if(ESPReply.indexOf(waitingFor) > 0){
                        isCommandSent =  true;
                        if(isCommandSent){
                          Serial.println(F("---------------- Got Reply TRUE--------------"));
                          Serial.println(ESPReply);
                          ESPReply = "";
                          Serial.println(F("---------------- END Reply TRUE--------------"));
                            return true;
                        }
                }              
            }
          Serial.println(F("---------------- Got Reply --------------"));
          Serial.println(ESPReply);
          ESPReply = "";
         Serial.println(F("---------------- END Reply --------------"));
          
        }

      return false;
  }

bool sendCommand(String c, unsigned long timeout, uint8_t retries, const char* waitingFor){

    return sendCommand(c.c_str(),timeout,retries,waitingFor);
}

