
/*
* Getting Started example sketch for nRF24L01+ radios
* This is a very basic example of how to send data from one node to another
* Updated: Dec 2014 by TMRh20
*/

#include <SPI.h>
#include "RF24.h"

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/
//bool radioNumber = 0;

/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(9,10);
/**********************************************************/

byte addresses[][6] = {"1Node","2Node"};

// Used to control whether this node is sending or receiving
bool role = 0;

// data Structure to send

/*
struct data {
    unsigned long _micros;
    String message;  
  } mydata;
*/

String message = "";

void setup() {
  Serial.begin(9600);
  Serial.println(F("RF24/examples/GettingStarted"));
  Serial.println(F("*** PRESS 'T' to begin transmitting to the other node"));
  
  radio.begin();

  // Set the PA Level low to prevent power supply related issues since this is a
 // getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_LOW);
  
  // Open a writing and reading pipe on each radio, with opposite addresses


  //radio.enableDynamicPayloads();
  radio.setChannel(108);
  radio.setDataRate(RF24_250KBPS);
  radio.openReadingPipe(1,addresses[0]);
  delay(1);
  // Start the radio listening for data
  radio.startListening();
}

void loop() {

  if(radio.available()){
    Serial.println(F("Got Data!"));
    message="";
      while(radio.available()){
          //Serial.print(F("."));
          radio.read(&message,1);          
          delay(1);
        }
        Serial.print(F("Total Length of message = "));
        Serial.println(message.length());
        //while(Serial.read());
       Serial.flush();
       Serial.print(F("Data Received :"));
       for(int i = 0 ; i < message.length() ; i ++){
          Serial.print((char)message[i]);
          Serial.print(i);
       }
       Serial.println();
       Serial.flush();
      // message[] = {0};
    }
//delay(10);
} // Loop

