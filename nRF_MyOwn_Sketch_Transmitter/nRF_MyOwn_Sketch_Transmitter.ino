
/*
* Getting Started example sketch for nRF24L01+ radios
* This is a very basic example of how to send data from one node to another
* Updated: Dec 2014 by TMRh20
*/

#include <SPI.h>
#include "RF24.h"

/****************** User Config ***************************/
/***      Set this radio as radio number 0 or 1         ***/
bool radioNumber = 1;

/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(9,10);
/**********************************************************/

byte addresses[][6] = {"1Node","2Node"};

// Used to control whether this node is sending or receiving
bool role = 0;

// data Structure to send
/*
struct data {
    unsigned long _micros;
    String message;  
  } mydata;
*/
String message;

void setup() {
  Serial.begin(9600);
  Serial.println(F("RF24/examples/GettingStarted"));
  Serial.println(F("*** PRESS 'T' to begin transmitting to the other node"));
  
  radio.begin();

  // Set the PA Level low to prevent power supply related issues since this is a
 // getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_LOW);
  
  // Open a writing and reading pipe on each radio, with opposite addresses
  //radio.enableDynamicPayloads();
  radio.setChannel(108);
  radio.setDataRate(RF24_250KBPS); 
  radio.openWritingPipe(addresses[0]);

}

void loop() {
  
  if(Serial.available() > 0){
    message="";
    Serial.println(F("Sending Data"));
    delay(10);
      while(Serial.available()){
          //Serial.println(F("Receiving Data"));
          message += (char)Serial.read();
          delay(1);
        }
        //while(Serial.read());
        Serial.println(message);
        Serial.flush();
        delay(10);
        
        for(int i = 0 ; i < message.length(); i++){
            int charToSend[1];
            charToSend[0]= message.charAt(i);
            radio.write( charToSend, 1);
            delay(1);
            Serial.print(i);   
        }
      delay(10);
    }else {
       // mydata.message = "No Message";  
       // radio.write(&mydata, sizeof(mydata));
    }
    delay(100);
} // Loop

