#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>

/*
This sketch sends a string to a corresponding Arduino
with nrf24 attached.  It appends a specific value 
(2 in this case) to the end to signify the end of the
message.
*/

#include "DHT.h"

#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);


struct {
   float f = 0;
   float h = 0;
   float t = 0;
  } myData;

int msg[1];
RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
void setup(void){
  Serial.begin(9600);
  radio.begin();
  dht.begin();

  radio.openWritingPipe(pipe);}
void loop(void){
 /* String theMessage = "";
  
   if(Serial.available() > 0){    
    Serial.print(F("Sending Data: "));
    delay(1);
      while(Serial.available()){
          //Serial.println(F("Receiving Data"));
          theMessage += (char)Serial.read();
          delay(1);
        }
        //while(Serial.read());
        Serial.println(theMessage);
        Serial.flush();
        delay(1);
  
  
  int messageSize = theMessage.length();
  for (int i = 0; i < messageSize; i++) {
    int charToSend[1];
    charToSend[0] = theMessage.charAt(i);
    radio.write(charToSend,1);
  }  
//send the 'terminate string' value...  
  msg[0] = 2; 
  radio.write(msg,1);
/*delay sending for a short period of time.  radio.powerDown()/radio.powerupp
//with a delay in between have worked well for this purpose(just using delay seems to
//interrupt the transmission start). However, this method could still be improved
as I still get the first character 'cut-off' sometimes. I have a 'checksum' function
on the receiver to verify the message was successfully sent.
*/

readDTH();


  radio.powerDown(); 
  delay(1000);
  radio.powerUp();
 //  }
}

void readDTH(){
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  myData.h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  myData.t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  myData.f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(myData.h) || isnan(myData.t) || isnan(myData.f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  bool done = false;
while(!done){
  done = radio.write(&myData, sizeof(myData));
  if(done){
      Serial.println(F("Data Sent!"));
        myData.h =0;
        myData.t = 0;
        myData.f = 0;
       // radio.powerDown(); 
      //  delay(1000);
       // radio.powerUp();
      //  delay(1);
      break; 
    }else {
        Serial.println(F("Retrying !"));
        //radio.powerDown(); 
        //delay(1000);
       // radio.powerUp();
       // delay(1);        
   }
}
  
  //Serial.print("Humidity: ");
  //Serial.print(h);
  //Serial.print(" %\t");
  //Serial.print("Temperature: ");
  //Serial.print(t);
  //Serial.print(" *C ");
  //Serial.print(f);
  //Serial.print(" *F\t");
  //Serial.print("Heat index: ");
  //Serial.print(hic);
  //Serial.print(" *C ");
  //Serial.print(hif);
  //Serial.println(" *F");
    
  }

