#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

/*
This sketch receives strings from sending unit via nrf24 
and prints them out via serial.  The sketch waits until
it receives a specific value (2 in this case), then it 
prints the complete message and clears the message buffer.
*/

struct {
   float f = 0;
   float h = 0;
   float t = 0;
   uint8_t mic = 0;
   float co2 = 0;
  } myData;

int msg[1];
RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
int lastmsg = 1;
String theMessage = "";
void setup(void){
  Serial.begin(9600);
  radio.begin();
  radio.openReadingPipe(1,pipe);
  radio.startListening();
}
void loop(void){
/*  if (radio.available()){
    bool done = false;  
      done = 1; radio.read(msg, 1); 
      char theChar = msg[0];
      if (msg[0] != 2){
        theMessage.concat(theChar);
        }
      else {
       Serial.println(theMessage);
       theMessage= ""; 
      }
   }*/

   if(radio.available()){
      //bool done = false;
      while(radio.available()){
        radio.read(&myData, sizeof(myData));
         
                 Serial.print("Humidity: ");
                 Serial.print(myData.h);
                  Serial.print(" %\t");
                  Serial.print("Temperature: ");
                  Serial.print(myData.t);
                  Serial.print(" *C ");
                  Serial.print(myData.f);
                  Serial.println(" *F\t");
                  Serial.print(F("Co2: "));
                  Serial.print(myData.co2);
                  Serial.print(F("mg/L \t"));
                  Serial.print(F("Noise: "));
                  Serial.println(myData.mic);
                  
                  break;
                  Serial.flush();
            
        }
        
    }
}
