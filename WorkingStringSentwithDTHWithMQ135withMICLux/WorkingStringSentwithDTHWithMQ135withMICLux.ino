#include <MQ135.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>

/*
This sketch sends a string to a corresponding Arduino
with nrf24 attached.  It appends a specific value 
(2 in this case) to the end to signify the end of the
message.
*/

#include "DHT.h"

#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

// These constants, define values needed for the LDR readings and ADC
#define LDR_PIN                   A3
#define MAX_ADC_READING           1023
#define ADC_REF_VOLTAGE           5.0
#define REF_RESISTANCE            5030  // measure this for best results
#define LUX_CALC_SCALAR           12518931
#define LUX_CALC_EXPONENT         -1.405



MQ135 mq(A0);

struct {
   float f = 0;
   float h = 0;
   float t = 0;
   uint8_t mic = 0;
   float co2 = 0;
   float ldrLux = 0;
  } myData;

RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;
void setup(void){
  Serial.begin(9600);
  radio.begin();
   radio.setPALevel(RF24_PA_MAX);
  dht.begin();
  pinMode(A1,INPUT);
  radio.openWritingPipe(pipe);}
void loop(void){
 /* String theMessage = "";
  
   if(Serial.available() > 0){    
    Serial.print(F("Sending Data: "));
    delay(1);
      while(Serial.available()){
          //Serial.println(F("Receiving Data"));
          theMessage += (char)Serial.read();
          delay(1);
        }
        //while(Serial.read());
        Serial.println(theMessage);
        Serial.flush();
        delay(1);
  
  
  int messageSize = theMessage.length();
  for (int i = 0; i < messageSize; i++) {
    int charToSend[1];
    charToSend[0] = theMessage.charAt(i);
    radio.write(charToSend,1);
  }  
//send the 'terminate string' value...  
  msg[0] = 2; 
  radio.write(msg,1);
/*delay sending for a short period of time.  radio.powerDown()/radio.powerupp
//with a delay in between have worked well for this purpose(just using delay seems to
//interrupt the transmission start). However, this method could still be improved
as I still get the first character 'cut-off' sometimes. I have a 'checksum' function
on the receiver to verify the message was successfully sent.
*/

myData.mic = map(analogRead(A1), 1023, 0, 0 , 500 );
readDTH();


  radio.powerDown(); 
  delay(1000);
  radio.powerUp();
 //  }
}

void readDTH(void){
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  myData.h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  myData.t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  myData.f = dht.readTemperature(true);
  myData.co2 = mq.getCorrectedPPM(myData.t,myData.h);
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(myData.h) || isnan(myData.t) || isnan(myData.f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  bool done = false;
while(!done){
  done = radio.write(&myData, sizeof(myData));
  if(done){
      Serial.println(F("Data Sent!"));
        Serial.print(F("The CO2 mg/L: "));
        Serial.println(myData.co2);
        myData.h =0;
        myData.t = 0;
        myData.f = 0;
        myData.co2 = 0;   
        getLdrData();
        break; 
    }else {
        Serial.println(F("Retrying !")); 
        delay(100)                 ;
   }
}

}


void getLdrData(void) 
{  
  
  int   ldrRawData;
  float resistorVoltage, ldrVoltage;
  float ldrResistance;
  float ldrLux;
  
  // Perform the analog to digital conversion  
  ldrRawData = analogRead(LDR_PIN);
  
  // RESISTOR VOLTAGE_CONVERSION
  // Convert the raw digital data back to the voltage that was measured on the analog pin
  resistorVoltage = (float)ldrRawData / MAX_ADC_READING * ADC_REF_VOLTAGE;

  // voltage across the LDR is the 5V supply minus the 5k resistor voltage
  ldrVoltage = ADC_REF_VOLTAGE - resistorVoltage;
  
  // LDR_RESISTANCE_CONVERSION
  // resistance that the LDR would have for that voltage  
  ldrResistance = ldrVoltage/resistorVoltage * REF_RESISTANCE;
  
  // LDR_LUX
  // Change the code below to the proper conversion from ldrResistance to
  // ldrLux
  myData.ldrLux = LUX_CALC_SCALAR * pow(ldrResistance, LUX_CALC_EXPONENT);
  // print out the results
//  Serial.print("LDR Raw Data   : "); Serial.println(ldrRawData);
//  Serial.print("LDR Voltage    : "); Serial.print(ldrVoltage); Serial.println(" volts");
//  Serial.print("LDR Resistance : "); Serial.print(ldrResistance); Serial.println(" Ohms");
//  Serial.print("LDR Illuminance: "); Serial.print(ldrLux); Serial.println(" lux");

}

void getBMPData(void){
  
  
  }
