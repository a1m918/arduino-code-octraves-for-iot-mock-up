
#include <stdint.h>
#include <MQ135.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <LowPower.h>
#include "SparkFunBME280.h"
//Library allows either I2C or SPI, so include both.
#include "Wire.h"
//#include "SPI.h"
/*
This sketch sends a string to a corresponding Arduino
with nrf24 attached.  It appends a specific value 
(2 in this case) to the end to signify the end of the
message.
*/

#include "DHT.h"

#define DHTPIN 4     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

// These constants, define values needed for the LDR readings and ADC
#define LDR_PIN                   A3
#define MAX_ADC_READING           1023
#define ADC_REF_VOLTAGE           5.0
#define REF_RESISTANCE            5030  // measure this for best results
#define LUX_CALC_SCALAR           12518931
#define LUX_CALC_EXPONENT         -1.405


BME280 mySensor;
MQ135 mq(A0);

struct {
   //float f = 0;
   float h = 0;
   float t = 0;
   uint8_t mic = 0;
   float co2 = 0;
   float ldrLux = 0;
   float BMP = 0;
  } myData;

RF24 radio(9,10);
const uint64_t pipe = 0xE8E8F0F0E1LL;

/*
idle(period_t period, adc_t adc, timer2_t timer2, 
                 timer1_t timer1, timer0_t timer0, spi_t spi,
                   usart0_t usart0, twi_t twi);
*/

void setup(void){
  Serial.begin(9600);

  ///////////////////////////////////// NRF ////////////////////////
   radio.begin();
   radio.setPALevel(RF24_PA_MAX);
  radio.openWritingPipe(pipe);
    //////////////////////////////////// NRF-END ////////////////////
  
  /////////////////////////////////// DTH ////////////////////////
  dht.begin();
  pinMode(A1,INPUT);
  ////////////////////////////////// DTH-END /////////////////////
  
  ////////////////////////////////// BMP ////////////////////////
  //BMP For I2C, enable the following and disable the SPI section
  mySensor.settings.commInterface = I2C_MODE;
  mySensor.settings.I2CAddress = 0x76;
 //renMode can be:
  //  0, Sleep mode
  //  1 or 2, Forced mode
  //  3, Normal mode
  mySensor.settings.runMode = 3; //Normal mode
  mySensor.settings.tStandby = 0;
    mySensor.settings.filter = 0;
  mySensor.settings.tempOverSample = 1;
    mySensor.settings.pressOverSample = 1;
  mySensor.settings.humidOverSample = 1;
  mySensor.begin();
  /////////////////////////////// BMP-END /////////////////////

  ////////////////////////////// LOW POWER ///////////////////
 //LowPower.powerSave(SLEEP_120MS, ADC_ON, BOD_ON, TIMER2_ON);
  }
void loop(void){

LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
myData.mic = map(analogRead(A1), 1023, 0, 0 , 500 );
//LowPower.adcNoiseReduction(SLEEP_120MS, ADC_ON, TIMER2_OFF);
LowPower.powerSave(SLEEP_120MS, ADC_ON, BOD_OFF, TIMER2_ON);
readDTH();
getLdrData();
getBMPData();

  bool done = false;
while(!done){
  done = radio.write(&myData, sizeof(myData));
  if(done){
      Serial.println(F("----------------------------"));
        Serial.print(F("The CO2 mg/L: "));
        Serial.println(myData.co2);
        Serial.print("LDR Lux : "); Serial.print(myData.ldrLux); Serial.println(" Lux");
        Serial.print("BMP: "); Serial.print(myData.BMP); Serial.println(" Pa");

        myData.h =0;
        myData.t = 0;
        ////////myData.f = 0;
        myData.co2 = 0;   
        
        break; 
    }else {
        Serial.println(F("Retrying !")); 
        //LowPower.powerDown(SLEEP_120MS, ADC_OFF, BOD_OFF);
   }
}

  radio.powerDown(); 
  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
  radio.powerUp();
 //  }
}

void readDTH(void){
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  myData.h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  myData.t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //////myData.f = dht.readTemperature(true);
  myData.co2 = mq.getCorrectedPPM(myData.t,myData.h);
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(myData.h) || isnan(myData.t) ) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }


}


void getLdrData(void) 
{  
  //Serial.println(F("LDR Function is called."));
  int   ldrRawData;
  float resistorVoltage, ldrVoltage;
  float ldrResistance;
  //float ldrLux;
  
  // Perform the analog to digital conversion  
  ldrRawData = analogRead(LDR_PIN);
  
  // RESISTOR VOLTAGE_CONVERSION
  // Convert the raw digital data back to the voltage that was measured on the analog pin
  resistorVoltage = (float)ldrRawData / MAX_ADC_READING * ADC_REF_VOLTAGE;

  // voltage across the LDR is the 5V supply minus the 5k resistor voltage
  ldrVoltage = ADC_REF_VOLTAGE - resistorVoltage;
  
  // LDR_RESISTANCE_CONVERSION
  // resistance that the LDR would have for that voltage  
  ldrResistance = ldrVoltage/resistorVoltage * REF_RESISTANCE;
  
  // LDR_LUX
  // Change the code below to the proper conversion from ldrResistance to
  // ldrLux
  myData.ldrLux = LUX_CALC_SCALAR * pow(ldrResistance, LUX_CALC_EXPONENT);
  // print out the results
//  Serial.print("LDR Raw Data   : "); Serial.println(ldrRawData);
//  Serial.print("LDR Voltage    : "); Serial.print(ldrVoltage); Serial.println(" volts");
//  Serial.print("LDR Resistance : "); Serial.print(ldrResistance); Serial.println(" Ohms");

}

void getBMPData(void){
  
  myData.BMP = mySensor.readFloatAltitudeFeet();

  }
